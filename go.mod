module gitlab.com/beoran/useql

go 1.12

require (
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20190512091148-babf20351dd7 // indirect
	golang.org/x/sys v0.0.0-20190524122548-abf6ff778158 // indirect
	modernc.org/b v1.0.0 // indirect
	modernc.org/db v1.0.0 // indirect
	modernc.org/file v1.0.0 // indirect
	modernc.org/fileutil v1.0.0 // indirect
	modernc.org/golex v1.0.0 // indirect
	modernc.org/internal v1.0.0 // indirect
	modernc.org/lldb v1.0.0 // indirect
	modernc.org/mathutil v1.0.0 // indirect
	modernc.org/ql v1.0.0
	modernc.org/sortutil v1.0.0 // indirect
	modernc.org/strutil v1.0.0 // indirect
	modernc.org/zappy v1.0.0 // indirect
)
