# useql

An example of how to use Ql with Go modules.

Shell Interaction: 

```
$ go mod init gitlab.com/beoran/useql
go: creating new go.mod: module gitlab.com/beoran/useql
$ go build
> go: finding modernc.org/ql v1.0.0
> go: finding modernc.org/internal/buffer latest
> go: finding modernc.org/golex/lex latest
> go: finding github.com/remyoudompheng/bigfft latest
> go: downloading github.com/remyoudompheng/bigfft v0.0.0-20190512091148-babf20351dd7
> go: extracting github.com/remyoudompheng/bigfft v0.0.0-20190512091148-babf20351dd7
> go: finding github.com/edsrzf/mmap-go v1.0.0
> go: finding golang.org/x/sys/unix latest
> go: finding golang.org/x/sys latest
> go: downloading golang.org/x/sys v0.0.0-20190524122548-abf6ff778158
> go: extracting golang.org/x/sys v0.0.0-20190524122548-abf6ff778158
$ ls
> go.mod  go.sum  main.go  README.md  useql
```

